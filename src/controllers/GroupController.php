<?php

namespace Lutzen\Gatekeeper;

use Illuminate\Support\MessageBag;

class GroupController extends \BaseController {

	protected $groups;
	protected $resources;
	protected $validator;

	public function __construct(\Lutzen\Gatekeeper\Interfaces\Repositories\Groups $groups, \Lutzen\Gatekeeper\Interfaces\Validators\GroupValidator $validator, \Lutzen\Gatekeeper\Interfaces\Repositories\Resources $resources) {
		$this->groups = $groups;
		$this->resources = $resources;
		$this->validator = $validator;
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		if (\Request::ajax()) {
			
			// TODO: Fix sorting
			$columns = array(
				0 => 'id',
				1 => 'name',
				4 => 'updated_at',
			);
			
			$sortBy = null;
			if (isset($columns[\Input::get('iSortCol_0')])) {
				$sortBy = $columns[\Input::get('iSortCol_0')];
			}
			
			$groups = $this->groups->getAll(
				\Input::get('iDisplayStart'),
				\Input::get('iDisplayLength'),
				\Input::get('sSearch'),
				$sortBy,
				\Input::get('sSortDir_0', 'asc'),
				\Input::get('sSearch', null)
			);
			
			$jsonGroups = [];
			foreach ($groups['groups'] as $group) {
				$jsonGroups[] = [
					$group->getIdentifier(),
					link_to_route('group/edit', $group->getName(), array('id' => $group->getIdentifier())),
					$group->getUpdatedAt()->toDateTimeString(),
				];
			}
			
			$data = [
				'sEcho' => \Input::get('sEcho'),
				'iTotalRecords' => $groups['count'],
				'iTotalDisplayRecords' => $groups['count'],
				'aaData' => $jsonGroups,
			];
			
			return \Response::json($data);
		} else {
			$this->layout->content =  \View::make('gatekeeper::group.index');
		}
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$errors = \Input::old('errors');
		if (empty($errors)) {
			$errors = new MessageBag();
		}
		
		$this->layout->content =  \View::make('gatekeeper::group.edit', [
			'identifier' => null,
			'name' => \Input::get('name'),
			'resources' => $this->resources->all(),
			'selectedResources' => null,
			'errors' => $errors,
			'saved' => \Input::old('saved', false),
			'action' => route('group/store'),
		]);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$name = \Input::get('name');
		$resourceIds = \Input::get('resource_ids', array());
		
		$data = $this->validator->validateData($name, $resourceIds);
		if ($data->any()) {
			return \Redirect::route('group/create', array())->withInput([ 'errors' => $data, 'name' => $name ]);
		}
		
		$group = $this->groups->createGroup($name, $resourceIds);
		
		return \Redirect::route('group/edit', ['group' => $group->getIdentifier()])->withInput([ 'saved' => true]);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  \Lutzen\Gatekeeper\Interfaces\Models\Group  $group
	 * @return Response
	 */
	public function show(\Lutzen\Gatekeeper\Interfaces\Models\Group $group)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  \Lutzen\Gatekeeper\Interfaces\Models\Group  $group
	 * @return Response
	 */
	public function edit(\Lutzen\Gatekeeper\Interfaces\Models\Group $group)
	{	
		$errors = \Input::old('errors');
		if (empty($errors)) {
			$errors = new MessageBag();
		}
		
		$this->layout->content =  \View::make('gatekeeper::group.edit', [
			'identifier' => $group->getIdentifier(), 
			'name' => \Input::get('name', $group->getName()),
			'resources' => $this->resources->all(),
			'selectedResources' => $group->resources,
			'errors' => $errors,
			'saved' => \Input::old('saved', false),
			'action' => route('group/update', ['user' => $group->getIdentifier() ]),
		]);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Lutzen\Gatekeeper\Interfaces\Models\Group  $group
	 * @return Response
	 */
	public function update(\Lutzen\Gatekeeper\Interfaces\Models\Group $group)
	{
		$name = \Input::get('name');
		$resourceIds = \Input::get('resource_ids', array());
		
		$data = $this->validator->validateData($name, $resourceIds);
		if ($data->any()) {
			return \Redirect::route('group/edit', array('group' => $group->getIdentifier()))->withInput([ 'errors' => $data, 'name' => $name, 'resource_ids' => $resourceIds]);
		}
		
		$group->updateGroup($name, $resourceIds);
		
		return \Redirect::route('group/edit', ['group' => $group->getIdentifier()])->withInput([ 'saved' => true]);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}