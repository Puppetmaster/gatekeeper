<?php

namespace Lutzen\Gatekeeper;

use Illuminate\Support\MessageBag;

class UserController extends \BaseController {

	protected $users;
	protected $groups;
	protected $validator;

	public function __construct(\Lutzen\Gatekeeper\Interfaces\Repositories\Users $users, \Lutzen\Gatekeeper\Interfaces\Validators\UserValidator $validator, \Lutzen\Gatekeeper\Interfaces\Repositories\Groups $groups) {
		$this->users = $users;
		$this->validator = $validator;
		$this->groups = $groups;
	}

	public function login() {
        $errors = new MessageBag();

        if ($old = \Input::old('errors')) {
            $errors = $old;
        }

        $data = [
            'errors' => $errors
        ];

        if (\Input::server('REQUEST_METHOD') == 'POST') {
            $validator = \Validator::make(\Input::all(), [
                'username' => 'required',
                'password' => 'required'
            ]);

            if ($validator->passes()) {
                $credentials = [
                    'username' => \Input::get('username'),
                    'password' => \Input::get('password')
                ];
				
                if (\Auth::attempt($credentials, \Input::get('remember_me', false))) {
					$user = \Auth::user();
					$user->updateLastLogin();
					if (\Hash::needsRehash($user->password)) {
						/*
						$user->password = $credentials['password'];
						$user->save();
						*/
					}
                    return \Redirect::route('user/profile');
                }
            }
            
            $data['errors'] = new MessageBag([
                'password' => [
                    'Username and/or password invalid.'
                ]
            ]);

            $data['username'] = \Input::get('username');

            return \Redirect::route('user/login')
                ->withInput($data);
        }

		$this->layout->content =  \View::make('gatekeeper::user.login', $data);
	}

    public function logout()
    {
        \Auth::logout();
        return \Redirect::route('user/login');
    }
	
	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		if (\Request::ajax()) {
			
			// TODO: Fix sorting
			$columns = array(
				0 => 'id',
				1 => 'name',
				2 => 'username',
				3 => 'email',
				4 => 'last_login',
			);
			
			$sortBy = null;
			if (isset($columns[\Input::get('iSortCol_0')])) {
				$sortBy = $columns[\Input::get('iSortCol_0')];
			}
			
			$users = $this->users->getAll(
				\Input::get('iDisplayStart'),
				\Input::get('iDisplayLength'),
				\Input::get('sSearch'),
				$sortBy,
				\Input::get('sSortDir_0', 'asc'),
				\Input::get('sSearch', null)
			);
			
			$jsonUsers = [];
			foreach ($users['users'] as $user) {
				$lastLogin = $user->getLastLogin();
				$jsonUsers[] = [
					$user->getIdentifier(),
					link_to_route('user/edit', $user->getName(), array('id' => $user->getIdentifier())),
					$user->getUsername(),
					$user->getEmail(),
					$lastLogin->timestamp > 0 ? $lastLogin->toDateTimeString() : 'Never',
				];
			}
			
			$data = [
				'sEcho' => \Input::get('sEcho'),
				'iTotalRecords' => $users['count'],
				'iTotalDisplayRecords' => $users['count'],
				'aaData' => $jsonUsers,
			];
			
			return \Response::json($data);
		} else {
			$this->layout->content =  \View::make('gatekeeper::user.index');
		}
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		$errors = \Input::old('errors');
		if (empty($errors)) {
			$errors = new MessageBag();
		}
		
		$this->layout->content =  \View::make('gatekeeper::user.edit', [
			'identifier' => null,
			'name' => \Input::get('name'),
			'username' => \Input::get('username'),
			'password' => '',
			'email' => \Input::get('email'),
			'groups' => $this->groups->all(),
			'selectedGroups' => null,
			'errors' => $errors,
			'saved' => \Input::old('saved', false),
			'action' => route('user/store'),
		]);
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$name = \Input::get('name');
		$username = \Input::get('username');
		$email = \Input::get('email');
		$groupIds = \Input::get('group_ids', array());
		$password = \Input::get('password');
		
		$data = $this->validator->validateData($name, $password, $username, $email, $groupIds);
		if ($data->any()) {
			return \Redirect::route('user/create', array('groups' => $this->groups->all()))->withInput([ 'errors' => $data, 'name' => $name, 'password' => null, 'username' => $username, 'email' => $email, 'group_ids' => $groupIds]);
		}
		
		$user = $this->users->createUser($name, $password, $username, $email, $groupIds);
		
		return \Redirect::route('user/edit', ['user' => $user->getIdentifier()])->withInput(['saved' => true]);
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  \Lutzen\Gatekeeper\Interfaces\Models\User  $user
	 * @return Response
	 */
	public function show(\Lutzen\Gatekeeper\Interfaces\Models\User $user)
	{
		//
	}

	public function profile() {
		$this->layout->content =  \View::make('gatekeeper::user.profile', array('user' => \Auth::user()));
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  \Lutzen\Gatekeeper\Interfaces\Models\User  $user
	 * @return Response
	 */
	public function edit(\Lutzen\Gatekeeper\Interfaces\Models\User $user)
	{	
		$errors = \Input::old('errors');
		if (empty($errors)) {
			$errors = new MessageBag();
		}
		
		$this->layout->content =  \View::make('gatekeeper::user.edit', [
			'identifier' => $user->getIdentifier(), 
			'name' => \Input::get('name', $user->getName()),
			'username' => \Input::get('username', $user->getUsername()),
			'password' => null,
			'email' => \Input::get('email', $user->getEmail()),
			'groups' => $this->groups->all(),
			'selectedGroups' => $user->groups,
			'errors' => $errors,
			'saved' => \Input::old('saved', false),
			'action' => route('user/update', ['user' => $user->getIdentifier() ]),
		]);
	}
	
	/**
	 * Update the specified resource in storage.
	 *
	 * @param  \Lutzen\Gatekeeper\Interfaces\Models\User  $user
	 * @return Response
	 */
	public function update(\Lutzen\Gatekeeper\Interfaces\Models\User $user)
	{
		$name = \Input::get('name');
		$username = \Input::get('username');
		$email = \Input::get('email');
		$groupIds = \Input::get('group_ids', array());
		$password = \Input::get('password');
		if (empty($password)) {
			$password = null;
		}
		
		$data = $this->validator->validateData($name, $password, $username, $email, $groupIds);
		if ($data->any()) {
			return \Redirect::route('user/edit', array('user' => $user->getIdentifier()))->withInput([ 'errors' => $data, 'name' => $name, 'username' => $username, 'email' => $email, 'group_ids' => $groupIds]);
		}
		
		$user->updateUser($name, $password, $username, $email, $groupIds);
		
		return \Redirect::route('user/edit', ['user' => $user->getIdentifier()])->withInput([ 'saved' => true]);
	}
	
	/**
	 * Show the form for editing the current user
	 *
	 * @return Response
	 */
	public function selfEdit()
	{	
		
		$user = \Auth::user();
		$errors = \Input::old('errors');
		if (empty($errors)) {
			$errors = new MessageBag();
		}
		
		$this->layout->content =  \View::make('gatekeeper::user.self_edit', [
			'identifier' => $user->getIdentifier(), 
			'name' => \Input::get('name', $user->getName()),
			'username' => \Input::get('username', $user->getUsername()),
			'password' => null,
			'email' => \Input::get('email', $user->getEmail()),
			'groups' => $this->groups->all(),
			'selectedGroups' => $user->groups,
			'errors' => $errors,
			'saved' => \Input::old('saved', false),
		]);
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @return Response
	 */
	public function selfUpdate()
	{
		$user = \Auth::user();
		$name = \Input::get('name');
		$username = \Input::get('username');
		$email = \Input::get('email');
		$password = \Input::get('password');
		if (empty($password)) {
			$password = null;
		}
		
		$data = $this->validator->validateData($name, $password, $username, $email);
		if ($data->any()) {
			return \Redirect::route('user/self-edit', array('user' => $user->getIdentifier()))->withInput([ 'errors' => $data, 'name' => $name, 'username' => $username, 'email' => $email ]);
		}
		
		$user->updateUser($name, $password, $username, $email);
		
		return \Redirect::route('user/self-edit')->withInput([ 'saved' => true]);
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}