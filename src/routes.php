<?php

Route::model('user', '\Lutzen\Gatekeeper\Models\User'); // FIXME: This is an ugly hack
Route::model('group', '\Lutzen\Gatekeeper\Models\Group'); // FIXME: This is an ugly hack

/*
Route::get('user/{user}', [
	'as' => 'user/profile',
	'uses' => 'UserController@show']
);

Route::get('user/profile', [
	'as' => 'user/profile',
	'uses' => 'UserController@show']
);



Route::get('user/logout', [
	'as' => 'user/logout',
	'uses' => 'UserController@showProfile']
);
Route::get('user/login', [
	'as' => 'user/login',
	'uses' => 'UserController@showProfile']
);


// User list
Route::get('users', [
	'as' => 'user/index',
	'uses' => 'Lutzen\Gatekeeper\UserController@index']
);

// Edit user
Route::get('user/edit/{user}', [
	'as' => 'user/edit',
	'uses' => 'Lutzen\Gatekeeper\UserController@edit']
);

// Update user
Route::post('user/update/{user}', [
	'as' => 'user/update',
	'uses' => 'Lutzen\Gatekeeper\UserController@update']
);

// Create new user
Route::get('gatekeeper/create-user', [
	'as' => 'user/create',
	'uses' => 'Lutzen\Gatekeeper\UserController@create']
);

// Store user
Route::post('user/store', [
	'as' => 'user/store',
	'uses' => 'Lutzen\Gatekeeper\UserController@store']
);

*/