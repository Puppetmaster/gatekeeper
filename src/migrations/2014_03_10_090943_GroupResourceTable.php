<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class GroupResourceTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('group_resource', function($table) {
			$table->integer('group_id')->unsigned();
			$table->integer('resource_id')->unsigned();
			
			
			$table->foreign('group_id')->references('id')->on('group')->onDelete('cascade')->onUpdate('cascade');
			$table->foreign('resource_id')->references('id')->on('resource')->onDelete('cascade')->onUpdate('cascade');
			$table->timestamps();
			
			$table->softDeletes();
			$table->primary(array('group_id', 'resource_id', 'deleted_at'));
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('group_resource');
	}

}
