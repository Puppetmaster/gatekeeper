<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ResourceTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('resource', function($table) {
	    	$table->increments('id')->unsigned();
			$table->string('name');
			$table->string('friendly_name')->nullable();
			$table->string('pattern');
			$table->string('target');
			$table->boolean('secure');
			$table->timestamps();
			
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('resource');
	}

}
