<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class GroupUserTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('group_user', function($table) {
			$table->integer('group_id')->unsigned();
			$table->integer('user_id')->unsigned();
			
			$table->foreign('group_id')->references('id')->on('group')->onDelete('cascade')->onUpdate('cascade');
			$table->foreign('user_id')->references('id')->on('user')->onDelete('cascade')->onUpdate('cascade');
			$table->timestamps();
			
			$table->primary(array('group_id', 'user_id', 'deleted_at'));
			$table->softDeletes();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('group_user');
	}

}
