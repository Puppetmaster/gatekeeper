<?php

namespace Lutzen\Gatekeeper;

use Illuminate\Support\ServiceProvider;

class GatekeeperServiceProvider extends ServiceProvider {

	/**
	 * Indicates if loading of the provider is deferred.
	 *
	 * @var bool
	 */
	protected $defer = false;

	/**
	 * Bootstrap the application events.
	 *
	 * @return void
	 */
	public function boot()
	{
		$this->package('lutzen/gatekeeper', 'gatekeeper');
	}

	/**
	 * Register the service provider.
	 *
	 * @return void
	 */
	public function register()
	{
		$app = $this->app;

		// Users
        $app->bind('Lutzen\Gatekeeper\Interfaces\Repositories\Users', 'Lutzen\Gatekeeper\Models\User');
		$app->bind('Lutzen\Gatekeeper\Interfaces\Models\User', 'Lutzen\Gatekeeper\Models\User');

		$app->bind('Lutzen\Gatekeeper\Interfaces\Validators\UserValidator', function() use ($app)
		{
			return new \Lutzen\Gatekeeper\Validators\User\Validator(
			array(
				$app->make('Lutzen\Gatekeeper\Validators\User\Simple'),
			)
			);
		});
		
		// Groups
		$app->bind('Lutzen\Gatekeeper\Interfaces\Models\Group', 'Lutzen\Gatekeeper\Models\Group');
		$app->bind('Lutzen\Gatekeeper\Interfaces\Repositories\Groups', 'Lutzen\Gatekeeper\Models\Group');
		$app->bind('Lutzen\Gatekeeper\Interfaces\Validators\GroupValidator', function() use ($app)
		{
			return new \Lutzen\Gatekeeper\Validators\Group\Validator(
			array(
				$app->make('Lutzen\Gatekeeper\Validators\Group\Simple'),
			)
			);
		});

		// Resources
		$app->bind('Lutzen\Gatekeeper\Interfaces\Models\Resource', 'Lutzen\Gatekeeper\Models\Resource');
		$app->bind('Lutzen\Gatekeeper\Interfaces\Repositories\Resources', 'Lutzen\Gatekeeper\Models\Resource');
		
		$app->app['gatekeeper'] = $this->app->share(function ($app) {
			return new Gatekeeper;
		});
		
		$app->booting(function() {
		  $loader = \Illuminate\Foundation\AliasLoader::getInstance();
		  $loader->alias('Gatekeeper', 'Lutzen\Gatekeeper\Facades\Gatekeeper');
		});
		
		include __DIR__ . '/../../helpers.php';
		include __DIR__ . '/../../routes.php';
		
	}

	/**
	 * Get the services provided by the provider.
	 *
	 * @return array
	 */
	public function provides()
	{
		return array('gatekeeper');
	}

}