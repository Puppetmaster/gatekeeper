<?php
	
namespace Lutzen\Gatekeeper\Models;
	
class Route {
	public static function addSecureRoutes() {
		\Route::group(['before' => 'guest'], function()
		{
		    $resources = \Lutzen\Gatekeeper\Models\Resource::where('secure', false)->get();

		    foreach ($resources as $resource)
		    {
		        \Route::any($resource->pattern, [
		            'as'   => $resource->name,
		            'uses' => $resource->target
		        ]);
		    }
		});

		\Route::group(['before' => 'auth'], function()
		{
		    $resources = \Lutzen\Gatekeeper\Models\Resource::where('secure', true)->get();

		    foreach ($resources as $resource)
		    {
		        \Route::any($resource->pattern, [
		            'as'   => $resource->name,
		            'uses' => $resource->target
		        ]);
		    }
		});
	}
}