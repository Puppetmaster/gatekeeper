<?php

namespace Lutzen\Gatekeeper\Models;

use Illuminate\Auth\UserInterface;

class User extends \Eloquent implements \Lutzen\Gatekeeper\Interfaces\Repositories\Users, \Lutzen\Gatekeeper\Interfaces\Models\User, UserInterface {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'user';

    protected $guarded = [
        'id',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

	/**
	 * The attributes excluded from the model's JSON form.
	 *
	 * @var array
	 */
	protected $hidden = array('password');

	public function getDates() {
	    return array('last_login');
	}

	/**
	 * Get the unique identifier for the user.
	 *
	 * @return mixed
	 */
	public function getAuthIdentifier()
	{
		return $this->getKey();
	}

	/**
	 * Get the password for the user.
	 *
	 * @return string
	 */
	public function getAuthPassword()
	{
		return $this->password;
	}

	public function getRememberToken() {
		return $this->remember_me;		
	}
	
	public function setRememberToken($value) {
		$this->remember_me = $value;
		$this->save();
	}
	
	public function getRememberTokenName() {
		return 'remember_me';
	}
	
	public function getAll($offset, $number, $search, $sortBy, $sortOrder, $search) {
		$users = User::take($number)->skip($offset);
		
		if ($search) {
			$users->where(function($query) use($search) {
				$query->where('name', 'LIKE', '%' . $search . '%')
					->orWhere('username', 'LIKE', '%' . $search . '%')
					->orWhere('email', 'LIKE', '%' . $search . '%');
			});
		}
		
		if ($sortBy) {
			$users = $users->orderBy($sortBy, $sortOrder);
		}
		
		$users = $users->get();
		
		return [
			'count' => User::count(), // TODO Fix wrong count
			'users' => $users,
		];
	}

	public function getByUsername($username) {
		return self::where('username', '=', $username)->first();
	}

	public function getByEmail($email) {
		return self::where('email', '=', $email)->first();
	}

	public function getIdentifier() {
		return $this->id;
	}
	
	public function getName() {
		return $this->name;
	}
	
	public function getUsername() {
		return $this->username;
	}
	public function getEmail() {
		return $this->email;
	}
	
	public function getLastLogin() {
		return $this->last_login;
	}
	
	public function updateUser($name, $password, $username, $email, $groupIds = null) {
		$this->name = $name;
		if ($password) {
			$this->password = $password;
		}
		$this->username = $username;
		$this->email = $email;
		if ($groupIds !== null) {
			$this->groups()->sync($groupIds);
		}
		return $this->save();
	}
	
	public function setPasswordAttribute($password) {
		$this->attributes['password'] = \Hash::make($password);
	}
	
	public function updateLastLogin() {
		$this->last_login = \Carbon\Carbon::now()->toDateTimeString();
		$this->save();
	}
	
	public function createUser($name, $password, $username, $email, $groupIds) {
		// TODO Fix groups
		$user = $this->create(array(
			'name' => $name,
			'username' => $username,
			'email' => $email,
		));
		
		$user->password = $password;
		$user->groups()->sync($groupIds);
		$user->save();
		
		return $user;
	}
	
    public function groups() {
		return $this->belongsToMany('\Lutzen\Gatekeeper\Models\Group')->withTimestamps();
	}

}