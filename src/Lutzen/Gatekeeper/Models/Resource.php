<?php

namespace Lutzen\Gatekeeper\Models;

class Resource extends \Eloquent implements \Lutzen\Gatekeeper\Interfaces\Models\Resource, \Lutzen\Gatekeeper\Interfaces\Repositories\Resources {
    protected $table = 'resource';

    protected $softDelete = true;

    protected $guarded = [
        'id',
        'created_at',
        'updated_at',
        'deleted_at',
    ];

	public function getFriendlyName() {
		if (empty($this->friendly_name)) {
			return $this->name;
		}
		
		return $this->name . ' (' . $this->friendly_name . ')';
	}

    public function groups() {
        return $this->belongsToMany('\Lutzen\Gatekeeper\Models\Group')->withTimestamps();
    }
}