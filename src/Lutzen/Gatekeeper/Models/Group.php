<?php

namespace Lutzen\Gatekeeper\Models;

class Group extends \Eloquent implements \Lutzen\Gatekeeper\Interfaces\Models\Group, \Lutzen\Gatekeeper\Interfaces\Repositories\Groups {
    protected $table = 'group';

    protected $softDelete = true;

    protected $guarded = [
        'id',
        'created_at',
        'updated_at',
        'deleted_at'
    ];

	public function getIdentifier() {
		return $this->id;
	}
	
	public function getName() {
		return $this->name;
	}

	public function getUpdatedAt() {
		return $this->updated_at;
	}

    public function resources()
    {
        return $this->belongsToMany('Lutzen\Gatekeeper\Models\Resource')->withTimestamps();
    }

    public function users()
    {
        return $this->belongsToMany('User')->withTimestamps();
    }
	
	public function getAll($offset, $number, $search, $sortBy, $sortOrder, $search) {
		$groups = Group::take($number)->skip($offset);
		
		if ($search) {
			$groups->where(function($query) use($search) {
				$query->where('name', 'LIKE', '%' . $search . '%');
			});
		}
		
		if ($sortBy) {
			$groups = $groups->orderBy($sortBy, $sortOrder);
		}
		
		$groups = $groups->get();
		
		return [
			'count' => Group::count(), // TODO Fix wrong count
			'groups' => $groups,
		];
	}
	
	public function updateGroup($name, $resourceIds) {
		$this->name = $name;
		$this->resources()->sync($resourceIds);
		$this->save();
	}
	
	public function createGroup($name, $resourceIds) {
		$group = $this->create(array('name' => $name));
		$group->resources()->sync($resourceIds);
		return $group;
	}
}