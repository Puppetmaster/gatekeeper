<?php

namespace Lutzen\Gatekeeper\Interfaces\Repositories;

interface Users {
	
	public function getAll($offset, $number, $search, $sortBy, $sortOrder, $search);
	public function getByUsername($username);
	public function getByEmail($email);
	
	public function createUser($name, $password, $username, $email, $groupIds);
}