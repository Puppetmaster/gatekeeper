<?php

namespace Lutzen\Gatekeeper\Interfaces\Repositories;

interface Groups {
	
	public static function all();
	
	public function getAll($offset, $number, $search, $sortBy, $sortOrder, $search);
	
	public function createGroup($name, $resourceIds);
}