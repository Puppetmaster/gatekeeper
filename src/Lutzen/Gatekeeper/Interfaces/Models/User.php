<?php

namespace Lutzen\Gatekeeper\Interfaces\Models;

interface User {
	
	public function getIdentifier();
	public function getName();
	public function getUsername();
	public function getEmail();
	public function getLastLogin();
	public function updateLastLogin();

	public function updateUser($name, $password, $username, $email, $groupIds);
	
}