<?php

namespace Lutzen\Gatekeeper\Interfaces\Models;

interface Resource {
	
	public static function firstOrCreate(array $resource);
	public function getFriendlyName();
	
}