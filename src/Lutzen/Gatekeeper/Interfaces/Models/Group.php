<?php

namespace Lutzen\Gatekeeper\Interfaces\Models;

interface Group {
	
	public function getIdentifier();
	public function getName();
	public function getUpdatedAt();
	
	public function updateGroup($name, $resourceIds);
}