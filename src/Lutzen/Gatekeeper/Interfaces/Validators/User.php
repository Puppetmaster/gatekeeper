<?php namespace Lutzen\Gatekeeper\Interfaces\Validators;

interface User {
	public function validateName($name);
	public function validatePassword($password);
	public function validateUsername($username);
	public function validateEmail($email);
	public function validateGroupIds($groupIds);
}