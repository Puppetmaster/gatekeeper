<?php namespace Lutzen\Gatekeeper\Interfaces\Validators;

interface Group {
	public function validateName($name);
	public function validateResourceIds($resourceIds);
}