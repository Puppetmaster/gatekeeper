<?php namespace Lutzen\Gatekeeper\Interfaces\Validators;

interface UserValidator extends User {
	public function validateData($name, $password, $username, $email, $groupIds);
}