<?php namespace Lutzen\Gatekeeper\Interfaces\Validators;

interface GroupValidator extends Group {
	public function validateData($name, array $resourceIds);
}