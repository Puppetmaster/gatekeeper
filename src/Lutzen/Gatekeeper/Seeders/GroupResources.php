<?php

namespace Lutzen\Gatekeeper\Seeders;

use Illuminate\Database\Seeder;
use Page;

class GroupResources extends Seeder
{
    public function run()
    {
		
		foreach (\Lutzen\Gatekeeper\Models\Resource::get() as $resource) {
			foreach (\Lutzen\Gatekeeper\Models\Group::get() as $group) {
				$resource->groups()->save($group);
			}
		}
    }
}