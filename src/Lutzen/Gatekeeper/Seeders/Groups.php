<?php

namespace Lutzen\Gatekeeper\Seeders;

use Illuminate\Database\Seeder;
use Page;

class Groups extends Seeder
{
    public function run()
    {
		$this->command->info('Gatekeeper Groups');
        $groups = [
            [
                'name' => 'Administrators',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s'),
            ],
        ];

        \DB::table('group')->insert($groups);
    }
}