<?php

namespace Lutzen\Gatekeeper\Seeders;

use Illuminate\Database\Seeder;
use Page;

class Users extends Seeder
{
    public function run()
    {
        $users = [
            [
                'username' => 'cl',
				'name' => 'Carsten Grønbjerg Lützen',
                'password' => \Hash::make('123qwe'),
                'email'    => 'carsten@lutzen.dk',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s'),
            ],
            [
                'username' => 'dfj',
				'name' => 'Daniel Fentz Johansen',
                'password' => \Hash::make('123qwe'),
                'email'    => 'dfjohansen@gmail.com',
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s'),
            ]
        ];

        foreach ($users as $user) {
        	\Lutzen\Gatekeeper\Models\User::create($user);
        }
    }
}