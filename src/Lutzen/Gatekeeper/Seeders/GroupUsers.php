<?php
namespace Lutzen\Gatekeeper\Seeders;

use Illuminate\Database\Seeder;
use Page;

class GroupUsers extends Seeder
{
    public function run()
    {
		$this->command->info('Gatekeeper Group Users');
        $groups = [
            [
                'group_id' => 1,
				'user_id' => 1,
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s'),
            ],
            [
                'group_id' => 1,
				'user_id' => 2,
				'created_at' => date('Y-m-d H:i:s'),
				'updated_at' => date('Y-m-d H:i:s'),
            ],
        ];

		\DB::table('group_user')->insert($groups);
    }
}