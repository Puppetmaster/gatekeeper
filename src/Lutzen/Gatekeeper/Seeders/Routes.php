<?php

namespace Lutzen\Gatekeeper\Seeders;

use Illuminate\Database\Seeder;
use Page;

class Routes extends Seeder {

    public function run()
    {
		$this->command->info('Gatekeeper Resources');
        $resources = [
			// Users
            [
                'pattern' => 'login',
                'name'    => 'user/login',
                'target'  => 'Lutzen\Gatekeeper\UserController@login',
                'secure'  => false,
            ],
            [
                'pattern' => 'logout',
                'name'    => 'user/logout',
                'target'  => 'Lutzen\Gatekeeper\UserController@logout',
                'secure'  => true,
            ],
            [
                'pattern' => 'users',
                'name'    => 'user/index',
                'target'  => 'Lutzen\Gatekeeper\UserController@index',
                'secure'  => true,
            ],
            [
                'pattern' => 'user/edit/{user}',
                'name'    => 'user/edit',
                'target'  => 'Lutzen\Gatekeeper\UserController@edit',
                'secure'  => true,
            ],
            [
                'pattern' => 'user/update/{user}',
                'name'    => 'user/update',
                'target'  => 'Lutzen\Gatekeeper\UserController@update',
                'secure'  => true,
            ],
            [
                'pattern' => 'user-edit',
                'name'    => 'user/self-edit',
                'target'  => 'Lutzen\Gatekeeper\UserController@selfEdit',
                'secure'  => true,
            ],
            [
                'pattern' => 'user-store',
                'name'    => 'user/self-update',
                'target'  => 'Lutzen\Gatekeeper\UserController@selfUpdate',
                'secure'  => true,
            ],
            [
                'pattern' => 'gatekeeper/create-user',
                'name'    => 'user/create',
                'target'  => 'Lutzen\Gatekeeper\UserController@create',
                'secure'  => true,
            ],
            [
                'pattern' => 'user/store',
                'name'    => 'user/store',
                'target'  => 'Lutzen\Gatekeeper\UserController@store',
                'secure'  => true,
            ],
            [
                'pattern' => 'user/profile',
                'name'    => 'user/profile',
                'target'  => 'Lutzen\Gatekeeper\UserController@profile',
                'secure'  => true,
            ],
			
			
			// Groups
            [
                'pattern' => 'groups',
                'name'    => 'group/index',
                'target'  => 'Lutzen\Gatekeeper\GroupController@index',
                'secure'  => true,
            ],
            [
                'pattern' => 'group/edit/{group}',
                'name'    => 'group/edit',
                'target'  => 'Lutzen\Gatekeeper\GroupController@edit',
                'secure'  => true,
            ],
            [
                'pattern' => 'group/update/{group}',
                'name'    => 'group/update',
                'target'  => 'Lutzen\Gatekeeper\GroupController@update',
                'secure'  => true,
            ],
            [
                'pattern' => 'gatekeeper/create-group',
                'name'    => 'group/create',
                'target'  => 'Lutzen\Gatekeeper\GroupController@create',
                'secure'  => true,
            ],
            [
                'pattern' => 'group/store',
                'name'    => 'group/store',
                'target'  => 'Lutzen\Gatekeeper\GroupController@store',
                'secure'  => true,
            ],
        ];

        foreach ($resources as $resource)
        {
            \Lutzen\Gatekeeper\Models\Resource::create($resource);
        }

		$this->command->info('Resource table seeded!');
    }

}