<?php namespace Lutzen\Gatekeeper\Validators\User;

use Illuminate\Support\MessageBag;

class Validator implements \Lutzen\Gatekeeper\Interfaces\Validators\UserValidator {
	
	protected $validators = array();
	
	public function __construct(array $validators = array()) {
		$this->validators = $validators;
	}
	
	public function validateName($name) {
		$data = array('name' => array());
		foreach ($this->validators as $validator) {
			try {
				$validator->validateName($name);
			} catch (\Exception $e) {
				$data['name'][] = $e->getMessage();
			}
		}
		
		if (count($data['name'])) {
			return $data;
		}
		
		return array();
	}
	
	public function validatePassword($password) {
		$data = array('password' => array());
		foreach ($this->validators as $validator) {
			try {
				$validator->validatePassword($password);
			} catch (\Exception $e) {
				$data['password'][] = $e->getMessage();
			}
		}
		
		if (count($data['password'])) {
			return $data;
		}
		
		return array();
	}
	
	public function validateUsername($username) {
		$data = array('username' => array());
		foreach ($this->validators as $validator) {
			try {
				$validator->validateUsername($username);
			} catch (\Exception $e) {
				$data['username'][] = $e->getMessage();
			}
		}
		
		if (count($data['username'])) {
			return $data;
		}
		
		return array();
	}
	
	public function validateEmail($email) {
		$data = array('email' => array());
		foreach ($this->validators as $validator) {
			try {
				$validator->validateEmail($email);
			} catch (\Exception $e) {
				$data['email'][] = $e->getMessage();
			}
		}
		
		if (count($data['email'])) {
			return $data;
		}
		
		return array();
	}
	
	public function validateGroupIds($groupIds) {
		$data = array('group_ids' => array());
		foreach ($this->validators as $validator) {
			try {
				$validator->validateGroupIds($groupIds);
			} catch (\Exception $e) {
				$data['group_ids'][] = $e->getMessage();
			}
		}
		
		if (count($data['group_ids'])) {
			return $data;
		}
		
		return array();
	}
	
	public function validateData($name, $password, $username, $email, $groupIds = null) {
		$data = new MessageBag();
		
		$data->merge($this->validateName($name));
		if ($password !== null) {
			$data->merge($this->validatePassword($password));
		}
		$data->merge($this->validateUsername($username));
		$data->merge($this->validateEmail($email));
		if ($groupIds !== null) {
			$data->merge($this->validateGroupIds($groupIds));
		}
		
		return $data;
	}
	
}