<?php namespace Lutzen\Gatekeeper\Validators\User;

class Simple implements \Lutzen\Gatekeeper\Interfaces\Validators\User {
	
	public function validateName($name) {
		if (empty($name)) {
			throw new \Exception('Name may not be empty');
		}
	}
	
	public function validatePassword($password) {
		if (strlen($password) < 6) {
			throw new \Exception('Password may not be shorter than 6 characters');
		}
	}
	
	public function validateUsername($username) {
		if (empty($username)) {
			throw new \Exception('Username may not be empty');
		}
	}
	
	public function validateEmail($email) {
		if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
			throw new \Exception('Email is malformed');
		}
	}
	
	public function validateGroupIds($groupIds) {
		if (empty($groupIds)) {
			throw new \Exception('At least one group must be selected');
		}
		foreach ($groupIds as $groupId) {
			// TODO: Fix this
		}
	}
	
		
}