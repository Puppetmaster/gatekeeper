<?php namespace Lutzen\Gatekeeper\Validators\Group;

class Simple implements \Lutzen\Gatekeeper\Interfaces\Validators\Group {
	
	public function validateName($name) {
		if (empty($name)) {
			throw new \Exception('Name may not be empty');
		}
	}	
	
	public function validateResourceIds($resourceIds) {
		foreach ($resourceIds as $resourceId) {
			// TODO: Fix this
		}
	}
}