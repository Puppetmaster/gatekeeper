<?php namespace Lutzen\Gatekeeper\Validators\Group;

use Illuminate\Support\MessageBag;

class Validator implements \Lutzen\Gatekeeper\Interfaces\Validators\GroupValidator {
	
	protected $validators = array();
	
	public function __construct(array $validators = array()) {
		$this->validators = $validators;
	}
	
	public function validateName($name) {
		$data = array('name' => array());
		foreach ($this->validators as $validator) {
			try {
				$validator->validateName($name);
			} catch (\Exception $e) {
				$data['name'][] = $e->getMessage();
			}
		}
		
		if (count($data['name'])) {
			return $data;
		}
		
		return array();
	}
	
	public function validateResourceIds($resourceIds) {
		$data = array('resource_ids' => array());
		foreach ($this->validators as $validator) {
			try {
				$validator->validateResourceIds($resourceIds);
			} catch (\Exception $e) {
				$data['resource_ids'][] = $e->getMessage();
			}
		}
		
		if (count($data['resource_ids'])) {
			return $data;
		}
		
		return array();
	}
	
	public function validateData($name, array $resourceIds) {
		$data = new MessageBag();
		
		$data->merge($this->validateName($name));
		$data->merge($this->validateResourceIds($resourceIds));
		
		return $data;
	}
	
}