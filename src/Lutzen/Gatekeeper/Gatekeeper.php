<?php namespace Lutzen\Gatekeeper;
 
class Gatekeeper {
 	public static function addResource($pattern, $name, $target, $secure, $friendlyName = null) {
		$resource = \App::make('Lutzen\Gatekeeper\Models\Resource');
		return $resource->firstOrCreate(array(
			'pattern' => $pattern,
			'name' => $name,
			'friendly_name' => $friendlyName,
			'target' => $target,
			'secure' => $secure,
		)) != false;
 	}
	
	public function getUserByUsername($username) {
		$userRepository = \App::make('Lutzen\Gatekeeper\Interfaces\Repositories\Users');
		
		return $userRepository->getByUsername($username);
	}
	
	public function getUserByEmail($email) {
		$userRepository = \App::make('Lutzen\Gatekeeper\Interfaces\Repositories\Users');
		
		return $userRepository->getByEmail($email);
	}
 
}