@section('title')
@if ($name)
	Edit "{{{$name}}}"
@else
	Create User
@endif
@stop

@section('css')
@stop



@section('javascript-files')
@stop

@section('javascript')
@if ($saved)
<script>
	$( document ).ready(function() {
		$('#user-saved').slideDown();
		setTimeout(function() {
			$('#user-saved').slideUp();
		}, 5000 );
	});
</script>
@endif
@stop

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                @if ($name)
				Edit User
				@else
				Create User
				@endif
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
				@if ($saved)
				<div class="row">
					<div class="col-lg-12">
						<div id="user-saved" style="display:none;" class="alert alert-success">The user have been updated</div>
					</div>
				</div>
				@endif	
				<div class="row">
					<form role="form" method="post" action="{{{ $action }}}">
						<div class="col-lg-6">
					        <div class="form-group">
								{{ Form::label('name', 'Name') }}
								@if ($error = $errors->first('name'))
								<div class="alert alert-danger">{{{ $error }}}</div>
								@endif
								{{ Form::text('name', $name, array('placeholder' => 'Name of the user', 'class' => 'form-control')) }}
					        </div>
							
					        <div class="form-group">
								{{ Form::label('username', 'Username') }}
								@if ($error = $errors->first('username'))
								<div class="alert alert-danger">{{{ $error }}}</div>
								@endif
								{{ Form::text('username', $username, array('placeholder' => 'Username of the user', 'class' => 'form-control')) }}
					        </div>
							
					        <div class="form-group">
								{{ Form::label('email', 'Email') }}@if ($error = $errors->first('email'))
								<div class="alert alert-danger">{{{ $error }}}</div>
								@endif
								{{ Form::text('email', $email, array('placeholder' => 'Email of the user', 'class' => 'form-control')) }}
					        </div>
							
					        <div class="form-group">
								{{ Form::label('password', 'Password') }}
								@if ($error = $errors->first('password'))
								<div class="alert alert-danger">{{{ $error }}}</div>
								@endif
								{{ Form::password('password', array('class' => 'form-control')) }}
	
								
					        </div>
					        
					        <button type="submit" class="btn btn-default">Save User</button>
					        <button type="reset" class="btn btn-default">Reset Form</button>
						</div>
						<!-- /.col-lg-6 (nested) -->
						<div class="col-lg-6">
				    		<div class="form-group">
				            	<label>Groups</label>
								@if ($error = $errors->first('group_ids'))
								<div class="alert alert-danger">{{{ $error }}}</div>
								@endif
								@foreach ($groups as $group)
								<div class="checkbox">
			                    	<input type="checkbox" id="group-{{{ $group->getIdentifier()}}}" name="group_ids[]" value="{{{ $group->getIdentifier() }}}" 
									@if ($selectedGroups && $selectedGroups->contains($group->getIdentifier()))
										checked="checked"
										@endif
									>
									<label for="group-{{{ $group->getIdentifier()}}}">{{{ $group->getName() }}}</label>
								</div>
								@endforeach
							</div>
						</div>
						<!-- /.col-lg-6 (nested) -->
					</form>
				</div>
				<!-- /.row (nested) -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
@stop