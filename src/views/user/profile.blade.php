@section('title')
	Profile
@stop

@section('css')
@stop



@section('javascript-files')
@stop

@section('javascript')
@stop

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                Profile
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
				<div class="row">
					<div class="col-lg-12">
					    Welcome once more {{{ $user->getName() }}}.
					</div>
				</div>
				<!-- /.row (nested) -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
@stop