@section('title')
	Users
@stop

@section('css')
<link href="{{{ Config::get('app.url') }}}/css/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet">
@stop



@section('javascript-files')
<script src="{{{ Config::get('app.url') }}}/js/plugins/dataTables/jquery.dataTables.js"></script>
<script src="{{{ Config::get('app.url') }}}/js/plugins/dataTables/dataTables.bootstrap.js"></script>

@stop

@section('javascript')
<script>
    $(document).ready(function() {
        $('#users').dataTable({
			"bProcessing": true,
			"bServerSide": true,
			"sAjaxSource": "{{{ route('user/index') }}}"
		});
    });
</script>

@stop

@section('content')
<div class="row">
    <div class="col-lg-12">
		
		<div class="panel panel-default">
		    <div class="panel-heading">
		        Create New User
		    </div>
		    <!-- /.panel-heading -->
		    <div class="panel-body">
		        <p>
		            <a href="{{{ route('user/create') }}}" class="btn btn-primary btn-lg btn-block">Create new user</a>
		        </p>
		    </div>
		    <!-- /.panel-body -->
		</div>
		
        <div class="panel panel-default">
            <div class="panel-heading">
                Users
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="users">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Username</th>
                                <th>Email</th>
                                <th>Last Login</th>
                            </tr>
                        </thead>
						<tbody>
							<tr>
								<td colspan="5" class="dataTables_empty"><i class="fa fa-spinner fa-spin"></i> Loading data from server</td>
							</tr>
						</tbody>
                    </table>
                </div>
                <!-- /.table-responsive -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
@stop