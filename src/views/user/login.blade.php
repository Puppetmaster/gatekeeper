@extends('layouts.master')

@section('title')
	Login
@stop

@section('main-content')
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                <div class="login-panel panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">Please Sign In to {{{ Config::get('app.name') }}}</h3>
                    </div>
                    <div class="panel-body">
                        <form role="form" method="post" action="{{{ route('user/login') }}}" autocomplete="off">
					        @if ($error = $errors->first("password"))
								<div class="alert alert-danger alert-dismissable">
									<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
									{{{ $error }}}
								</div>
					        @endif
                            <fieldset>
                                <div class="form-group @if (!empty($error)) has-error @endif">
                                    <input class="form-control" placeholder="Username" name="username" type="username" autofocus value="{{{ \Input::get('username') }}}">
                                </div>
                                <div class="form-group @if (!empty($error)) has-error @endif">
                                    <input class="form-control" placeholder="Password" name="password" type="password" value="">
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input name="remember_me" type="checkbox" value="Remember Me">Remember Me
                                    </label>
                                </div>
                                <!-- Change this to a button or input when using this as a form -->
                                <button class="btn btn-lg btn-success btn-block">Login</button>
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@stop