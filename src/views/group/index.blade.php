@section('title')
	Users
@stop

@section('css')
<link href="{{{ Config::get('app.url') }}}/css/plugins/dataTables/dataTables.bootstrap.css" rel="stylesheet">
@stop



@section('javascript-files')
<script src="{{{ Config::get('app.url') }}}/js/plugins/dataTables/jquery.dataTables.js"></script>
<script src="{{{ Config::get('app.url') }}}/js/plugins/dataTables/dataTables.bootstrap.js"></script>

@stop

@section('javascript')
<script>
    $(document).ready(function() {
        $('#groups').dataTable({
			"bProcessing": true,
			"bServerSide": true,
			"sAjaxSource": "{{{ route('group/index') }}}"
		});
    });
</script>

@stop

@section('content')
<div class="row">
    <div class="col-lg-12">
		
		<div class="panel panel-default">
		    <div class="panel-heading">
		        Create New Group
		    </div>
		    <!-- /.panel-heading -->
		    <div class="panel-body">
		        <p>
		            <a href="{{{ route('group/create') }}}" class="btn btn-primary btn-lg btn-block">Create new group</a>
		        </p>
		    </div>
		    <!-- /.panel-body -->
		</div>
		
        <div class="panel panel-default">
            <div class="panel-heading">
                Groups
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" id="groups">
                        <thead>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Updated At</th>
                            </tr>
                        </thead>
						<tbody>
							<tr>
								<td colspan="3" class="dataTables_empty"><i class="fa fa-spinner fa-spin"></i> Loading data from server</td>
							</tr>
						</tbody>
                    </table>
                </div>
                <!-- /.table-responsive -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
@stop