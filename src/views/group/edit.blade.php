@section('title')
@if ($name)
	Edit "{{{$name}}}"
@else
	Create Group
@endif
@stop

@section('css')
@stop



@section('javascript-files')
@stop

@section('javascript')
@if ($saved)
<script>
	$( document ).ready(function() {
		$('#group-saved').slideDown();
		setTimeout(function() {
			$('#group-saved').slideUp();
		}, 5000 );
	});
</script>
@endif
@stop

@section('content')
<div class="row">
    <div class="col-lg-12">
        <div class="panel panel-default">
            <div class="panel-heading">
                @if ($name)
				Edit Group
				@else
				Create Group
				@endif
            </div>
            <!-- /.panel-heading -->
            <div class="panel-body">
				@if ($saved)
				<div class="row">
					<div class="col-lg-12">
						<div id="group-saved" style="display:none;" class="alert alert-success">The group have been updated</div>
					</div>
				</div>
				@endif	
				<div class="row">
					<form role="form" method="post" action="{{{ $action }}}">
						<div class="col-lg-6">
					        <div class="form-group">
								{{ Form::label('name', 'Name') }}
								{{ Form::text('name', $name, array('placeholder' => 'Name of the group', 'class' => 'form-control')) }}
	
								@if ($error = $errors->first('name'))
								<div class="alert alert-danger">{{{ $error }}}</div>
								@endif
					        </div>
					        
					        <button type="submit" class="btn btn-default">Save Group</button>
					        <button type="reset" class="btn btn-default">Reset Form</button>
						</div>
						<!-- /.col-lg-6 (nested) -->
						<div class="col-lg-6">
			    		<div class="form-group">
			            	<label>Resources</label>
							@foreach ($resources as $resource)
							<div class="checkbox">
		                    	<input type="checkbox" id="resource-{{{ $resource->id}}}" name="resource_ids[]" value="{{{ $resource->id }}}" 
								@if ($selectedResources && $selectedResources->contains($resource->id))
									checked="checked"
									@endif
								>
								<label for="resource-{{{ $resource->id}}}">{{{ $resource->getFriendlyName() }}}</label>
							</div>
							@endforeach
						</div>
						</div>
						<!-- /.col-lg-6 (nested) -->
					</form>
				</div>
				<!-- /.row (nested) -->
            </div>
            <!-- /.panel-body -->
        </div>
        <!-- /.panel -->
    </div>
    <!-- /.col-lg-12 -->
</div>
<!-- /.row -->
@stop